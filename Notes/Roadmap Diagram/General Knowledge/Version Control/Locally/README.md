## Version Control - Locally

**Here you will learn about:**

- Git;

**Actually not local:**

- There are actually local version control systems;
- But the best one, and the most popular one is actually a **Distributed Version Control**;
  ![Version Control Image](https://github.com/JoaoGuimaraes22/2020-Todo/blob/master/img/distributedversioncontrol.png)

**Links:**

- https://en.wikipedia.org/wiki/Version_control#Specialized_strategies ;
- https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control ;
- https://blog.eduonix.com/software-development/learn-three-types-version-control-systems/ ;
