## C# / .NET

**What you will learn is:**

- Learn the language;
- Frameworks and tools:
  - .NET Core;
  - .NET Framework;
  - ASP.NET;
  - Entity Framework;
  - LINQ;
  - Others;
