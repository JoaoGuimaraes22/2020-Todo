## Functional Language

**What you will learn is:**

- C# / .NET:
  - Learn the language;
  - Frameworks and tools:
    - .NET Core;
    - .NET Framework;
    - ASP.NET;
    - Entity Framework;
    - LINQ;
    - Others;
- Java (opt.);
  - Learn the language;
